package routes

import (
	c "bookmanagement/controllers"

	"github.com/labstack/echo"
)

func AuthorSubroute(g *echo.Group) {
	g.GET("", c.GetAuthors)
	g.GET("/:id", c.GetAuthor)
	g.POST("", c.AddAuthor)
	g.PUT("/:id", c.UpdateAuthor)
	g.DELETE("/:id", c.DeleteAuthor)
}
