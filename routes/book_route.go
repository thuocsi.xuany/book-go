package routes

import (
	c "bookmanagement/controllers"

	"github.com/labstack/echo"
)

func BookSubroute(g *echo.Group) {
	g.GET("/author/:author_id", c.GetBooksOfAuthor)
	g.GET("/category/:category_id", c.GetBooksOfCategory)
	g.GET("", c.GetBooksByPage)

	g.GET("/:id", c.GetBook)
	g.POST("", c.AddBook)
	g.PUT("/:id", c.UpdateBook)
	g.DELETE("/:id", c.DeleteBook)
}
