package routes

import (
	c "bookmanagement/controllers"

	"github.com/labstack/echo"
)

func CategorySubroute(g *echo.Group) {
	g.GET("", c.GetCategories)
	g.GET("/:id", c.GetCategory)
	g.POST("", c.AddCategory)
	g.PUT("/:id", c.UpdateCategory)
	g.DELETE("/:id", c.DeleteCategory)

}
