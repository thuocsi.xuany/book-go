package routes

import (
	c "bookmanagement/controllers"
	m "bookmanagement/middlewares"

	"github.com/labstack/echo"
)

func UserSubroute(g *echo.Group) {

	g.POST("/login", c.Login)
	g.POST("/register", c.Register)
	g.GET("/logout", c.Logout, m.VerifyToken)
	g.GET("/refresh_token", c.RefreshToken)

	g.GET("/profile", c.GetProfile, m.VerifyToken)
	g.PUT("/profile", c.UpdateProfile, m.VerifyToken)
	g.PUT("/reset-password", c.ResetPassword)
	g.PUT("/change-password", c.ChangePassword, m.VerifyToken)
	g.GET("/:username/reset-password", c.SendResetPasswordLink)
	// g.DELETE("", c.DeleteAccount, m.VerifyToken)
}
