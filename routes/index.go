package routes

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func Route() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	authorGroup := e.Group("/authors")
	categoryGroup := e.Group("/categories")
	bookGroup := e.Group("/books")
	userGroup := e.Group("/user")

	AuthorSubroute(authorGroup)
	CategorySubroute(categoryGroup)
	BookSubroute(bookGroup)
	UserSubroute(userGroup)

	// start server
	e.Logger.Fatal(e.Start(":1234"))
}
