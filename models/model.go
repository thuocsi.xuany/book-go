package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name         string             `json:"name,omitempty" bson:"name,omitempty" validate:"required|minLen:3|maxLen:50" message:"required:name is required|minLen:name must be at least 3 characters|maxLen:name must be less than 50 characters"`
	Email        string             `json:"email,omitempty" bson:"email,omitempty" validate:"required|email" message:"email:email is invalid"`
	Username     string             `json:"username,omitempty" bson:"username,omitempty" validate:"required|minLen:5|maxLen:50" message:"required:username is required|minLen:username must be at least 5 characters|maxLen:username must be less than 50 characters"`
	Password     string             `json:"password,omitempty" bson:"password,omitempty" validate:"required|minLen:6|maxLen:50" message:"required:password is required|minLen:password must be at least 6 characters|maxLen:password must be less than 50 characters"`
	RefreshToken string             `json:"refresh_token,omitempty" bson:"refresh_token,omitempty"`
	LastUpdate   time.Time          `json:"last_update,omitempty" bson:"last_update,omitempty"`
}

type Category struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name        *string            `json:"name,omitempty" bson:"name,omitempty"`
	Description string             `json:"description,omitempty" bson:"description,omitempty"`
	LastUpdate  time.Time          `json:"last_update,omitempty" bson:"last_update,omitempty"`
}

type Author struct {
	ID         primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name       *string            `json:"name,omitempty" bson:"name,omitempty"`
	Avatar     string             `json:"avatar,omitempty" bson:"avatar,omitempty"`
	LastUpdate time.Time          `json:"last_update,omitempty" bson:"last_update,omitempty"`
}

type Book struct {
	ID          primitive.ObjectID   `json:"_id,omitempty" bson:"_id,omitempty"`
	Title       *string              `json:"title,omitempty" bson:"title,omitempty"`
	Description string               `json:"description,omitempty" bson:"description,omitempty"`
	Year        *int                 `json:"year,omitempty" bson:"year,omitempty"`
	Price       float64              `json:"price,omitempty" bson:"price,omitempty"`
	Cover       string               `json:"image,omitempty" bson:"image,omitempty"`
	CategoryIDs []primitive.ObjectID `json:"category_ids,omitempty" bson:"category_ids,omitempty"`
	AuthorIDs   []primitive.ObjectID `json:"author_ids,omitempty" bson:"author_ids,omitempty"`
	Categories  []*Category          `json:"categories,omitempty" bson:"-"`
	Authors     []*Author            `json:"authors,omitempty" bson:"-"`
	LastUpdate  time.Time            `json:"last_update,omitempty" bson:"last_update,omitempty"`
}

type BookAuthor struct {
	ID         primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	BookID     primitive.ObjectID `json:"book_id,omitempty" bson:"book_id,omitempty"`
	AuthorID   primitive.ObjectID `json:"author_id,omitempty" bson:"author_id,omitempty"`
	LastUpdate time.Time          `json:"last_update,omitempty" bson:"last_update,omitempty"`
}

type CartItem struct {
	ID         primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	BookID     primitive.ObjectID `json:"book_id,omitempty" bson:"book_id,omitempty"`
	Quantity   int                `json:"quantity,omitempty" bson:"quantity,omitempty"`
	Price      float64            `json:"price,omitempty" bson:"price,omitempty"`
	LastUpdate time.Time          `json:"last_update,omitempty" bson:"last_update,omitempty"`
}

type Cart struct {
	ID         primitive.ObjectID   `json:"_id,omitempty" bson:"_id,omitempty"`
	UserID     primitive.ObjectID   `json:"user_id,omitempty" bson:"user_id,omitempty"`
	Items      []*CartItem          `json:"items,omitempty" bson:"items,omitempty"`
	ItemIDs    []primitive.ObjectID `json:"item_ids,omitempty" bson:"item_ids,omitempty"`
	LastUpdate time.Time            `json:"last_update,omitempty" bson:"last_update,omitempty"`
}
