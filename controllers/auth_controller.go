package controllers

import (
	. "bookmanagement/config"
	. "bookmanagement/models"
	"context"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/gookit/validate"
	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
)

func Register(c echo.Context) error {
	var user User
	if err := c.Bind(&user); err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// Validate
	v := validate.Struct(user)

	if !v.Validate() {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: v.Errors.One(),
		})
	}

	// check if username is already taken
	var checkDupUser User
	if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": user.Username}).Decode(&checkDupUser); err == nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: DuplicatedUsernameErrorMsg,
		})
	}

	// check if email is already taken
	if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"email": user.Email}).Decode(&checkDupUser); err == nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: DuplicatedEmailErrorMsg,
		})
	}

	// Hash password
	hashPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	user.Password = string(hashPassword)
	user.LastUpdate = time.Now()

	// Save user
	insertResult, err := GlobalUserCollection.InsertOne(context.TODO(), user)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusCreated, insertResult)
}

func Login(c echo.Context) error {
	// Bind
	var user User
	if err := c.Bind(&user); err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// Validate
	if user.Username == "" {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: RequiredUsernameMsg,
		})
	}
	if user.Password == "" {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: RequiredPasswordMsg,
		})
	}

	// Find user
	var findUser User
	if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": user.Username}).Decode(&findUser); err != nil {
		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: NotFoundUsernameErrorMsg,
		})
	}

	// Compare password
	if err := bcrypt.CompareHashAndPassword([]byte(findUser.Password), []byte(user.Password)); err != nil {
		return c.JSON(http.StatusUnauthorized, Response{
			Status:  http.StatusUnauthorized,
			Message: IncorrectPasswordErrorMsg,
		})
	}

	// Create token
	accessTokenString, refreshTokenString := GenerateToken(findUser)

	if accessTokenString == "" || refreshTokenString == "" {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: CouldNotLoginMsg,
		})
	}

	// Update refresh token
	if err := UpdateRefreshToken(findUser.Username, refreshTokenString); err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusOK, Token{
		AccessToken:  accessTokenString,
		RefreshToken: refreshTokenString,
	})
}

func RefreshToken(c echo.Context) error {
	// Get token from header
	refreshToken := c.Request().Header.Get("Authorization")
	refreshToken = strings.Split(refreshToken, "Bearer ")[1]

	// Validate token
	token, err := jwt.Parse(refreshToken, func(token *jwt.Token) (interface{}, error) {

		return []byte(os.Getenv("REFRESH_TOKEN_SECRET")), nil
	})

	if err != nil {
		return c.JSON(http.StatusUnauthorized, Response{
			Status:  http.StatusUnauthorized,
			Message: err.Error(),
		})
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		username := claims["username"].(string)

		// Find user
		var findUser User
		if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&findUser); err != nil {
			return c.JSON(http.StatusNotFound, Response{
				Status:  http.StatusNotFound,
				Message: NotFoundUsernameErrorMsg,
			})
		}

		// Create token
		accessTokenString, refreshTokenString := GenerateToken(findUser)

		if accessTokenString == "" || refreshTokenString == "" {
			return c.JSON(http.StatusInternalServerError, Response{
				Status:  http.StatusInternalServerError,
				Message: AnErrorOccurredErrorMsg,
			})
		}

		return c.JSON(http.StatusOK, Token{
			AccessToken:  accessTokenString,
			RefreshToken: refreshTokenString,
		})
	}

	return c.JSON(http.StatusUnauthorized, Response{
		Status:  http.StatusUnauthorized,
		Message: InvalidTokenErrorMsg,
	})

}

func Logout(c echo.Context) error {

	username := c.Get("username").(string)

	var user User
	if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&user); err != nil {
		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: NotFoundUsernameErrorMsg,
		})
	}

	if user.RefreshToken == "" {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: UserAlreadyLoggedOutErrorMsg,
		})
	}

	if err := UpdateRefreshToken(username, ""); err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusOK, Response{
		Status:  http.StatusOK,
		Message: LoggedOutMsg,
	})
}

func UpdateRefreshToken(username string, refreshToken string) error {
	_, err := GlobalUserCollection.UpdateOne(context.TODO(), bson.M{"username": username}, bson.M{"$set": bson.M{"refresh_token": refreshToken}})

	if err != nil {
		return err
	}

	return nil
}

func GenerateToken(user User) (string, string) {
	// access token
	accessToken := jwt.New(jwt.SigningMethodHS256)

	claims := accessToken.Claims.(jwt.MapClaims)
	claims["username"] = user.Username
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()

	accessTokenString, err := accessToken.SignedString([]byte(os.Getenv("ACCESS_TOKEN_SECRET")))

	if err != nil {
		return "", ""
	}

	// refresh token
	refreshToken := jwt.New(jwt.SigningMethodHS256)

	claims = refreshToken.Claims.(jwt.MapClaims)
	claims["username"] = user.Username
	claims["exp"] = time.Now().Add(time.Hour * 24 * 7).Unix()

	refreshTokenString, err := refreshToken.SignedString([]byte(os.Getenv("REFRESH_TOKEN_SECRET")))

	if err != nil {
		return "", ""
	}

	return accessTokenString, refreshTokenString
}
