package controllers

import (
	"context"
	"math"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	. "bookmanagement/config"
	. "bookmanagement/models"
)

// GetBooksByPage godoc
// @Summary Get books by page
// @Description Get books with pagination
// @Tags books
// @Accept  json
// @Produce  json
// @Param page query int true "Page number" mininum(1)
// @Success 200 {array} Book
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /books [get]
func GetBooksByPage(c echo.Context) error {
	var page int
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidPageErrorMsg,
		})
	}

	if page < 1 {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidPageErrorMsg,
		})
	}

	var books []Book

	totalBooks, err := GlobalBookCollection.CountDocuments(context.TODO(), bson.M{})

	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	start := int64((page - 1) * int(PageSize))
	end := int64(math.Min(float64(start+PageSize), float64(totalBooks)))

	altualSize := end - start

	if start >= totalBooks {
		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: NotFoundPageErrorMsg,
		})
	}

	cursor, err := GlobalBookCollection.Find(context.TODO(), bson.D{}, &options.FindOptions{
		Skip:  &start,
		Limit: &altualSize,
		Sort:  bson.D{{"last_update", -1}},
	})

	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	defer cursor.Close(context.TODO())

	for cursor.Next(context.TODO()) {
		var book Book
		err := cursor.Decode(&book)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, Response{
				Status:  http.StatusInternalServerError,
				Message: err.Error(),
			})
		}
		books = append(books, book)
	}

	for i, book := range books {
		// get ref data
		for _, author_id := range book.AuthorIDs {
			var author Author
			err := GlobalAuthorCollection.FindOne(context.TODO(), bson.M{"_id": author_id}).Decode(&author)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, Response{
					Status:  http.StatusInternalServerError,
					Message: err.Error(),
				})
			}
			book.Authors = append(book.Authors, &author)
		}

		for _, category_id := range book.CategoryIDs {
			var category Category
			err := GlobalCategoryCollection.FindOne(context.TODO(), bson.M{"_id": category_id}).Decode(&category)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, Response{
					Status:  http.StatusInternalServerError,
					Message: err.Error(),
				})
			}
			book.Categories = append(book.Categories, &category)
		}
		books[i] = book
	}

	return c.JSON(http.StatusOK, books)
}

// GetBooksOfAuthor godoc
// @Summary Get books of author	by id
// @Description Get books of author by id
// @Tags books
// @Accept  json
// @Produce  json
// @Param author_id path string true "Author ID" minlength(24) maxlength(24)
// @Success 200 {array} Book
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /books/author/{author_id} [get]
func GetBooksOfAuthor(c echo.Context) error {
	author_id, err := primitive.ObjectIDFromHex(c.Param("author_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidIDErrorMsg,
		})
	}

	// find book in book_author
	var bookAuthors []BookAuthor
	cursor, err := GlobalBookAuthorCollection.Find(context.TODO(), bson.M{"author_id": author_id})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	defer cursor.Close(context.TODO())

	for cursor.Next(context.TODO()) {
		var bookAuthor BookAuthor
		err := cursor.Decode(&bookAuthor)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, Response{
				Status:  http.StatusInternalServerError,
				Message: err.Error(),
			})
		}
		bookAuthors = append(bookAuthors, bookAuthor)
	}

	var books []Book

	for _, bookAuthor := range bookAuthors {
		var book Book
		err := GlobalBookCollection.FindOne(context.TODO(), bson.M{"_id": bookAuthor.BookID}).Decode(&book)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, Response{
				Status:  http.StatusInternalServerError,
				Message: err.Error(),
			})
		}
		books = append(books, book)
	}

	for i, book := range books {
		// get ref data
		for _, author_id := range book.AuthorIDs {
			var author Author
			err := GlobalAuthorCollection.FindOne(context.TODO(), bson.M{"_id": author_id}).Decode(&author)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, Response{
					Status:  http.StatusInternalServerError,
					Message: err.Error(),
				})
			}
			book.Authors = append(book.Authors, &author)
		}

		for _, category_id := range book.CategoryIDs {
			var category Category
			err := GlobalCategoryCollection.FindOne(context.TODO(), bson.M{"_id": category_id}).Decode(&category)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, Response{
					Status:  http.StatusInternalServerError,
					Message: err.Error(),
				})
			}
			book.Categories = append(book.Categories, &category)
		}
		books[i] = book
	}

	return c.JSON(http.StatusOK, books)
}

// GetBooksOfCategory godoc
// @Summary Get books of category	by id
// @Description Get books of category by id
// @Tags books
// @Accept  json
// @Produce  json
// @Param category_id path string true "Category ID" minlength(24) maxlength(24)
// @Success 200 {array} Book
// @Failure 400 {object} Response
// @Failure 500 {object} Response
// @Router /books/category/{category_id} [get]
func GetBooksOfCategory(c echo.Context) error {
	category_id, err := primitive.ObjectIDFromHex(c.Param("category_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidIDErrorMsg,
		})
	}

	// find book belong to category
	var books []Book
	cursor, err := GlobalBookCollection.Find(context.TODO(), bson.M{"category_ids": bson.M{"$in": []primitive.ObjectID{category_id}}})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	defer cursor.Close(context.TODO())

	for cursor.Next(context.TODO()) {
		var book Book
		err := cursor.Decode(&book)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, Response{
				Status:  http.StatusInternalServerError,
				Message: err.Error(),
			})
		}
		books = append(books, book)
	}

	for i, book := range books {
		// get ref data
		for _, author_id := range book.AuthorIDs {
			var author Author
			err := GlobalAuthorCollection.FindOne(context.TODO(), bson.M{"_id": author_id}).Decode(&author)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, Response{
					Status:  http.StatusInternalServerError,
					Message: err.Error(),
				})
			}
			book.Authors = append(book.Authors, &author)
		}

		for _, category_id := range book.CategoryIDs {
			var category Category
			err := GlobalCategoryCollection.FindOne(context.TODO(), bson.M{"_id": category_id}).Decode(&category)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, Response{
					Status:  http.StatusInternalServerError,
					Message: err.Error(),
				})
			}
			book.Categories = append(book.Categories, &category)
		}
		books[i] = book
	}

	return c.JSON(http.StatusOK, books)
}

// AddBook godoc
// @Summary Add book
// @Description Add book
// @Tags books
// @Accept  json
// @Produce  json
// @Param book body Book true "Book"
// @Success 201 {object} InsertOneResult
// @Failure 400 {object} Response
// @Failure 500 {object} Response
// @Router /books [post]
func AddBook(c echo.Context) error {
	var book Book

	if err := c.Bind(&book); err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// check input valid
	if book.Title == nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: MissingTitleErrorMsg,
		})
	}

	if *book.Title == "" {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: EmptyTitleErrorMsg,
		})
	}

	if len(*book.Title) > MaxTitleLength {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: TitleTooLongErrorMsg,
		})
	}

	var checkDupBook Book
	err := GlobalBookCollection.FindOne(context.TODO(), bson.M{"title": *book.Title}).Decode(&checkDupBook)
	if err == nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: DuplicatedTitleErrorMsg,
		})
	}

	if book.Year != nil && (*book.Year < 1500 || *book.Year > time.Now().Year()) {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidYearErrorMsg,
		})
	}

	if book.Price < 0 {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidPriceErrorMsg,
		})
	}

	if _, err := url.Parse(book.Cover); err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidCoverErrorMsg,
		})
	}

	if book.AuthorIDs != nil {
		for _, author_id := range book.AuthorIDs {
			var author Author
			err := GlobalAuthorCollection.FindOne(context.TODO(), bson.M{"_id": author_id}).Decode(&author)
			if err == mongo.ErrNoDocuments {
				return c.JSON(http.StatusBadRequest, Response{
					Status:  http.StatusBadRequest,
					Message: NonExistentAuthorErrorMsg,
				})
			}
			if err != nil {
				return c.JSON(http.StatusInternalServerError, Response{
					Status:  http.StatusInternalServerError,
					Message: err.Error(),
				})
			}
		}
	}

	if book.CategoryIDs != nil {
		for _, category_id := range book.CategoryIDs {
			var category Category
			err := GlobalCategoryCollection.FindOne(context.TODO(), bson.M{"_id": category_id}).Decode(&category)
			if err == mongo.ErrNoDocuments {
				return c.JSON(http.StatusBadRequest, Response{
					Status:  http.StatusBadRequest,
					Message: NonExistentCategoryErrorMsg,
				})
			}
			if err != nil {
				return c.JSON(http.StatusInternalServerError, Response{
					Status:  http.StatusInternalServerError,
					Message: err.Error(),
				})
			}

		}
	}

	book.LastUpdate = time.Now()

	// insert book
	insertResult, err := GlobalBookCollection.InsertOne(context.TODO(), book)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// insert book_author
	if book.AuthorIDs != nil {
		for _, author_id := range book.AuthorIDs {
			bookAuthor := BookAuthor{
				BookID:     insertResult.InsertedID.(primitive.ObjectID),
				AuthorID:   author_id,
				LastUpdate: time.Now(),
			}
			_, err := GlobalBookAuthorCollection.InsertOne(context.TODO(), bookAuthor)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, Response{
					Status:  http.StatusInternalServerError,
					Message: err.Error(),
				})
			}
		}
	}

	return c.JSON(http.StatusCreated, insertResult)
}

// GetBook godoc
// @Summary Get book
// @Description Get book
// @Tags books
// @Accept  json
// @Produce  json
// @Param id path string true "Book ID"
// @Success 200 {object} Book
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /books/{id} [get]
func GetBook(c echo.Context) error {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidIDErrorMsg,
		})
	}

	var book Book
	err = GlobalBookCollection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&book)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return c.JSON(http.StatusNotFound, Response{
				Status:  http.StatusNotFound,
				Message: NotFoundBookIDErrorMsg,
			})
		}
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// get ref data
	for _, author_id := range book.AuthorIDs {
		var author Author
		err := GlobalAuthorCollection.FindOne(context.TODO(), bson.M{"_id": author_id}).Decode(&author)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, Response{
				Status:  http.StatusInternalServerError,
				Message: err.Error(),
			})
		}
		book.Authors = append(book.Authors, &author)
	}

	for _, category_id := range book.CategoryIDs {
		var category Category
		err := GlobalCategoryCollection.FindOne(context.TODO(), bson.M{"_id": category_id}).Decode(&category)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, Response{
				Status:  http.StatusInternalServerError,
				Message: err.Error(),
			})
		}
		book.Categories = append(book.Categories, &category)
	}

	return c.JSON(http.StatusOK, book)
}

// UpdateBook godoc
// @Summary Update book
// @Description Update book
// @Tags books
// @Accept  json
// @Produce  json
// @Param id path string true "Book ID"
// @Param book body Book true "Book"
// @Success 202 {object} UpdateOneResult
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /books/{id} [put]
func UpdateBook(c echo.Context) error {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	var book Book
	if err := c.Bind(&book); err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// check input valid

	// check id book

	var bookCheck Book
	if err = GlobalBookCollection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&bookCheck); err != nil {
		if err == mongo.ErrNoDocuments {
			return c.JSON(http.StatusNotFound, Response{
				Status:  http.StatusNotFound,
				Message: NotFoundBookIDErrorMsg,
			})
		}
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	if book.Title != nil && *book.Title == "" {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: EmptyTitleErrorMsg,
		})
	}

	if book.Title != nil && len(*book.Title) > MaxTitleLength {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: TitleTooLongErrorMsg,
		})
	}

	if book.Year != nil && (*book.Year < 1500 || *book.Year > time.Now().Year()) {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidYearErrorMsg,
		})
	}

	if book.Price < 0 {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidPriceErrorMsg,
		})
	}

	if book.Cover != "" {
		if _, err := url.Parse(book.Cover); err != nil {
			return c.JSON(http.StatusBadRequest, Response{
				Status:  http.StatusBadRequest,
				Message: InvalidCoverErrorMsg,
			})
		}
	}

	if book.AuthorIDs != nil {
		for _, author_id := range book.AuthorIDs {
			var author Author
			err := GlobalAuthorCollection.FindOne(context.TODO(), bson.M{"_id": author_id}).Decode(&author)
			if err == mongo.ErrNoDocuments {
				return c.JSON(http.StatusBadRequest, Response{
					Status:  http.StatusBadRequest,
					Message: NonExistentAuthorErrorMsg,
				})
			}
		}
	}

	if book.CategoryIDs != nil {
		for _, category_id := range book.CategoryIDs {
			var category Category
			err := GlobalCategoryCollection.FindOne(context.TODO(), bson.M{"_id": category_id}).Decode(&category)
			if err == mongo.ErrNoDocuments {
				return c.JSON(http.StatusBadRequest, Response{
					Status:  http.StatusBadRequest,
					Message: NonExistentCategoryErrorMsg,
				})
			}
		}
	}

	book.LastUpdate = time.Now()

	// update book
	updateResult, err := GlobalBookCollection.UpdateOne(context.TODO(), bson.M{"_id": id}, bson.M{"$set": book})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// update book_author
	if book.AuthorIDs != nil {
		// remove old author_book
		_, err := GlobalBookAuthorCollection.DeleteMany(context.TODO(), bson.M{"book_id": id})
		if err != nil {
			return c.JSON(http.StatusInternalServerError, Response{
				Status:  http.StatusInternalServerError,
				Message: err.Error(),
			})
		}

		// add new author_book
		for _, author_id := range book.AuthorIDs {
			bookAuthor := BookAuthor{
				BookID:     id,
				AuthorID:   author_id,
				LastUpdate: time.Now(),
			}
			_, err := GlobalBookAuthorCollection.InsertOne(context.TODO(), bookAuthor)
			if err != nil {
				return c.JSON(http.StatusInternalServerError, Response{
					Status:  http.StatusInternalServerError,
					Message: err.Error(),
				})
			}
		}
	}

	return c.JSON(http.StatusAccepted, updateResult)
}

// DeleteBook godoc
// @Summary Delete book
// @Description Delete book
// @Tags books
// @Accept  json
// @Produce  json
// @Param id path string true "Book ID"
// @Success 202 {object} DeleteOneResult
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /books/{id} [delete]
func DeleteBook(c echo.Context) error {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: InvalidIDErrorMsg,
		})
	}

	var book Book
	err = GlobalBookCollection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&book)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return c.JSON(http.StatusNotFound, Response{
				Status:  http.StatusNotFound,
				Message: NotFoundBookIDErrorMsg,
			})
		}
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	deleteResult, err := GlobalBookCollection.DeleteOne(context.TODO(), bson.M{"_id": id})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// delete book_author
	_, err = GlobalBookAuthorCollection.DeleteMany(context.TODO(), bson.M{"book_id": id})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusAccepted, deleteResult)
}
