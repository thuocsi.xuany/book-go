package controllers

import (
	"context"
	"net/http"
	"time"

	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	. "bookmanagement/config"
	. "bookmanagement/models"
)

// GetAuthors godoc
// @Summary Get all authors
// @Description Get all authors
// @Tags author
// @Accept  json
// @Produce  json
// @Success 200 {array} Author
// @Failure 500 {object} Response
// @Router /authors [get]
func GetAuthors(c echo.Context) error {
	var authors []Author

	cursor, err := GlobalAuthorCollection.Find(context.TODO(), bson.D{},
		&options.FindOptions{
			Sort: bson.D{{"last_update", -1}},
		})

	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}
	defer cursor.Close(context.TODO())

	for cursor.Next(context.TODO()) {
		var author Author
		err := cursor.Decode(&author)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, Response{
				Status:  http.StatusInternalServerError,
				Message: err.Error(),
			})
		}
		authors = append(authors, author)
	}

	return c.JSON(http.StatusOK, authors)

}

// AddAuthor godoc
// @Summary Add author
// @Description Add author
// @Tags author
// @Accept  json
// @Produce  json
// @Param author body Author true "Author"
// @Success 201 {object} InsertOneResult
// @Failure 400 {object} Response
// @Failure 500 {object} Response
// @Router /authors [post]
func AddAuthor(c echo.Context) error {
	var author Author

	if err := c.Bind(&author); err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// check input valid
	if author.Name == nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: MissingNameErrorMsg,
		})
	}

	if *author.Name == "" {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: EmptyNameErrorMsg,
		})
	}

	// set default value
	if author.Avatar == "" {
		author.Avatar = DefaultAvatar
	}
	author.LastUpdate = time.Now()

	// add
	insertResult, err := GlobalAuthorCollection.InsertOne(context.TODO(), author)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusCreated, insertResult)
}

// GetAuthor godoc
// @Summary Get author
// @Description Get author
// @Tags author
// @Accept  json
// @Produce  json
// @Param id path string true "Author ID"
// @Success 200 {object} Author
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /authors/{id} [get]
func GetAuthor(c echo.Context) error {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	var author Author
	if err := GlobalAuthorCollection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&author); err != nil {
		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusOK, author)
}

// UpdateAuthor godoc
// @Summary Update author
// @Description Update author
// @Tags author
// @Accept  json
// @Produce  json
// @Param id path string true "Author ID"
// @Param author body Author true "Author"
// @Success 202 {object} UpdateOneResult
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /authors/{id} [put]
func UpdateAuthor(c echo.Context) error {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	var author Author
	if err := c.Bind(&author); err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	var oldAuthor Author
	if err := GlobalAuthorCollection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&oldAuthor); err != nil {
		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		})
	}

	// check input
	if author.Name != nil && *author.Name == "" {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: EmptyNameErrorMsg,
		})
	}

	// update
	author.LastUpdate = time.Now()
	updateResult, err := GlobalAuthorCollection.UpdateOne(context.TODO(), bson.M{"_id": id}, bson.M{"$set": author})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusAccepted, updateResult)

}

// DeleteAuthor godoc
// @Summary Delete author
// @Description Delete author
// @Tags author
// @Accept  json
// @Produce  json
// @Param id path string true "Author ID"
// @Success 202 {object} DeleteOneResult
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /authors/{id} [delete]
func DeleteAuthor(c echo.Context) error {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	var author Author
	err = GlobalAuthorCollection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&author)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return c.JSON(http.StatusNotFound, Response{
				Status:  http.StatusNotFound,
				Message: NotFoundAuthorIDErrorMsg,
			})
		}

		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		})
	}

	deleteResult, err := GlobalAuthorCollection.DeleteOne(context.TODO(), bson.M{"_id": id})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// remove author id in book collection
	GlobalBookCollection.UpdateMany(context.TODO(), bson.M{}, bson.M{"$pull": bson.M{"author_ids": id}})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// remove author in book_author
	GlobalBookAuthorCollection.DeleteMany(context.TODO(), bson.M{"author_id": id})

	return c.JSON(http.StatusAccepted, deleteResult)
}
