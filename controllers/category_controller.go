package controllers

import (
	"context"
	"net/http"
	"time"

	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	. "bookmanagement/config"
	. "bookmanagement/models"
)

// GetCategories godoc
// @Summary Get all categories
// @Description Get all categories
// @Tags category
// @Accept  json
// @Produce  json
// @Success 200 {array} Category
// @Failure 400 {object} Response
// @Failure 500 {object} Response
// @Router /categories [get]
func GetCategories(c echo.Context) error {
	var categories []Category
	cursor, err := GlobalCategoryCollection.Find(context.TODO(), bson.D{},
		&options.FindOptions{
			Sort: bson.D{{"last_update", -1}},
		})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}
	defer cursor.Close(context.TODO())

	for cursor.Next(context.TODO()) {
		var category Category
		cursor.Decode(&category)
		categories = append(categories, category)
	}

	return c.JSON(http.StatusOK, categories)
}

// AddCategory godoc
// @Summary Add category
// @Description Add category
// @Tags category
// @Accept  json
// @Produce  json
// @Param category body Category true "Category"
// @Success 201 {object} InsertOneResult
// @Failure 400 {object} Response
// @Failure 500 {object} Response
// @Router /categories [post]
func AddCategory(c echo.Context) error {
	var category Category
	if err := c.Bind(&category); err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// check input valid
	if category.Name == nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: MissingNameErrorMsg,
		})
	}
	if *category.Name == "" {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: EmptyNameErrorMsg,
		})
	}

	// check duplicate name
	var checkCategory Category
	err := GlobalCategoryCollection.FindOne(context.TODO(), bson.M{"name": category.Name}).Decode(&checkCategory)
	if err == nil { // tìm thấy
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: DuplicatedNameErrorMsg,
		})
	}

	// set default value
	category.LastUpdate = time.Now()

	// add
	insertResult, err := GlobalCategoryCollection.InsertOne(context.TODO(), category)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusCreated, insertResult)
}

// GetCategory godoc
// @Summary Get category
// @Description Get category
// @Tags category
// @Accept  json
// @Produce  json
// @Param id path string true "Category ID"
// @Param category body Category true "Category"
// @Success 200 {object} Category
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /categories/{id} [get]
func GetCategory(c echo.Context) error {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	var category Category
	err = GlobalCategoryCollection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&category)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return c.JSON(http.StatusNotFound, Response{
				Status:  http.StatusNotFound,
				Message: NotFoundCategoryIDErrorMsg,
			})
		}

		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusOK, category)
}

// UpdateCategory godoc
// @Summary Update category
// @Description Update category
// @Tags category
// @Accept  json
// @Produce  json
// @Param id path string true "Category ID"
// @Param category body Category true "Category"
// @Success 202 {object} UpdateOneResult
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /categories/{id} [put]
func UpdateCategory(c echo.Context) error {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	var category Category
	if err := c.Bind(&category); err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// check input valid
	// check input
	if category.Name != nil && *category.Name == "" {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: EmptyNameErrorMsg,
		})
	}

	// check duplicate name
	var checkCategory Category
	err = GlobalCategoryCollection.FindOne(context.TODO(), bson.M{"name": category.Name}).Decode(&checkCategory)

	if err == nil { // tìm thấy
		if checkCategory.ID != id {
			return c.JSON(http.StatusBadRequest, Response{
				Status:  http.StatusBadRequest,
				Message: DuplicatedNameErrorMsg,
			})
		}
	}
	if err != mongo.ErrNoDocuments {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// update
	category.LastUpdate = time.Now()
	updateResult, err := GlobalCategoryCollection.UpdateOne(context.TODO(), bson.M{"_id": id}, bson.M{"$set": category})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusAccepted, updateResult)
}

// DeleteCategory godoc
// @Summary Delete category
// @Description Delete category
// @Tags category
// @Accept  json
// @Produce  json
// @Param id path string true "Category ID"
// @Success 202 {object} DeleteOneResult
// @Failure 400 {object} Response
// @Failure 404 {object} Response
// @Failure 500 {object} Response
// @Router /categories/{id} [delete]
func DeleteCategory(c echo.Context) error {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	// check category
	var category Category
	err = GlobalCategoryCollection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&category)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return c.JSON(http.StatusNotFound, Response{
				Status:  http.StatusNotFound,
				Message: NotFoundCategoryIDErrorMsg,
			})
		}

		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// delete
	deleteResult, err := GlobalCategoryCollection.DeleteOne(context.TODO(), bson.M{"_id": id})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// remove category id in book collection
	_, err = GlobalBookCollection.UpdateMany(context.TODO(), bson.M{}, bson.M{"$pull": bson.M{"category_ids": id}})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusAccepted, deleteResult)
}
