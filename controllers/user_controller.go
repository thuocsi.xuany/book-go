package controllers

import (
	. "bookmanagement/config"
	. "bookmanagement/models"
	"context"
	"net/http"
	"net/smtp"
	"os"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/gookit/validate"
	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
)

func GetProfile(c echo.Context) error {
	username := c.Get("username").(string)

	var user User
	err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": username}, options.FindOne().SetProjection(bson.M{"password": 0})).Decode(&user)
	if err != nil {
		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		})
	}
	return c.JSON(http.StatusOK, user)
}

func SendResetPasswordLink(c echo.Context) error {

	username := c.Param("username")

	from := os.Getenv("MAIL_USERNAME")
	password := os.Getenv("MAIL_PASSWORD")

	// find email user
	var user User
	if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&user); err != nil {
		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: NotFoundUsernameErrorMsg,
		})
	}

	toList := []string{user.Email}

	// Create token
	resetPwdToken := jwt.New(jwt.SigningMethodHS256)
	claims := resetPwdToken.Claims.(jwt.MapClaims)
	claims["username"] = username
	claims["exp"] = time.Now().Add(time.Hour * 100).Unix()
	resetPwdTokenString, err := resetPwdToken.SignedString([]byte(os.Getenv("RESET_PWD_TOKEN_SECRET")))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	addr := "smtp.gmail.com:587"
	host := "smtp.gmail.com"

	// send email
	subject := "Reset Password"
	body := "To reset your password, please click on the link below:\n\n" + os.Getenv("APP_URL") + "/user/reset-password?reset_token=" + resetPwdTokenString
	msg := []byte("From:" + from + "\r\nTo: " + toList[0] + "\r\n" + "Subject: " + subject + "\r\n\r\n" + body + "\r\n")
	auth := smtp.PlainAuth("", from, password, host)

	err = smtp.SendMail(addr, auth, from, toList, msg)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusAccepted, Response{
		Status:  http.StatusAccepted,
		Message: "Reset password link has been sent to your email",
	})
}

func ResetPassword(c echo.Context) error {
	// get token from query string
	resetToken := c.QueryParam("reset_token")

	// decode token
	token, err := jwt.Parse(resetToken, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("RESET_PWD_TOKEN_SECRET")), nil
	})
	if err != nil {
		return c.JSON(http.StatusUnauthorized, Response{
			Status:  http.StatusUnauthorized,
			Message: err.Error(),
		})
	}

	// get username from token
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}
	username := claims["username"].(string)

	// get new password from body
	var newPassword struct {
		NewPassword string `json:"new_password"`
	}
	if err := c.Bind(&newPassword); err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	// check username valid
	var user User
	if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&user); err != nil {
		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		})
	}
	// hash password
	hashPassword, err := bcrypt.GenerateFromPassword([]byte(newPassword.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// update password
	user.Password = string(hashPassword)
	_, err = GlobalUserCollection.UpdateOne(context.TODO(), bson.M{"username": username}, bson.M{"$set": bson.M{"password": user.Password}})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusAccepted, Response{
		Status:  http.StatusAccepted,
		Message: ResetPasswordMsg,
	})

}

func UpdateProfile(c echo.Context) error {
	username := c.Get("username").(string)

	var user UpdateUser
	if err := c.Bind(&user); err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// validate
	v := validate.Struct(user)
	if !v.Validate() {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: v.Errors.One(),
		})
	}

	var oldUser User
	if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&oldUser); err != nil {
		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		})
	}

	// update
	user.LastUpdate = time.Now()
	updateResult, err := GlobalUserCollection.UpdateOne(context.TODO(), bson.M{"username": username}, bson.M{"$set": user})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusAccepted, updateResult)
}

func ChangePassword(c echo.Context) error {
	username := c.Get("username").(string)

	var updatePassword struct {
		OldPassword string `json:"old_password"`
		NewPassword string `json:"new_password"`
	}

	if err := c.Bind(&updatePassword); err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
	}

	// check username valid
	var user User
	if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&user); err != nil {
		return c.JSON(http.StatusNotFound, Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		})
	}

	// check old password
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(updatePassword.OldPassword))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Response{
			Status:  http.StatusBadRequest,
			Message:IncorrectOldPasswordMsg,
		})
	}

	// hash password
	hashPassword, err := bcrypt.GenerateFromPassword([]byte(updatePassword.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	// update password
	user.Password = string(hashPassword)
	updateResult, err := GlobalUserCollection.UpdateOne(context.TODO(), bson.M{"username": username}, bson.M{"$set": bson.M{"password": user.Password}})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Response{
			Status:  http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	return c.JSON(http.StatusAccepted, updateResult)
}

// func DeleteAccount (c echo.Context) error {
// 	username := c.Get("username").(string)

// 	// check username valid
// 	var user User
// 	if err := GlobalUserCollection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&user); err != nil {
// 		return c.JSON(http.StatusNotFound, Response{
// 			Status:  http.StatusNotFound,
// 			Message: err.Error(),
// 		})
// 	}

// 	// delete user
// 	deleteResult, err := GlobalUserCollection.DeleteOne(context.TODO(), bson.M{"username": username})
// 	if err != nil {
// 		return c.JSON(http.StatusInternalServerError, Response{
// 			Status:  http.StatusInternalServerError,
// 			Message: err.Error(),
// 		})
// 	}

// 	return c.JSON(http.StatusAccepted, deleteResult)
// }
