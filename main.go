package main

import (
	"bookmanagement/config"
	"bookmanagement/routes"

	"github.com/joho/godotenv"
)

func main() {
	// ...
	err := godotenv.Load(".env")

	if err != nil {
		panic(err)
	}

	config.ConnectDB()
	routes.Route()
}
