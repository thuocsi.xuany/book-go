package config

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	GlobalClient               *mongo.Client
	GlobalAuthorCollection     *mongo.Collection
	GlobalBookCollection       *mongo.Collection
	GlobalCategoryCollection   *mongo.Collection
	GlobalBookAuthorCollection *mongo.Collection
	GlobalUserCollection       *mongo.Collection
	GlobalCartCollection       *mongo.Collection
	GlobalCartItemCollection   *mongo.Collection
)

func ConnectDB() {
	// connect to db
	clientOptions := options.Client().ApplyURI("mongodb+srv://ydam:ydam@notes-cluster.iwril.mongodb.net/BookManagement?retryWrites=true&w=majority")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	if err := client.Ping(context.TODO(), nil); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB!")
	GlobalClient = client

	// get collection
	GlobalAuthorCollection = client.Database("BookManagement").Collection("author")
	GlobalBookCollection = client.Database("BookManagement").Collection("book")
	GlobalCategoryCollection = client.Database("BookManagement").Collection("category")
	GlobalBookAuthorCollection = client.Database("BookManagement").Collection("book_author")
	GlobalUserCollection = client.Database("BookManagement").Collection("user")
	GlobalCartCollection = client.Database("BookManagement").Collection("cart")
	GlobalCartItemCollection = client.Database("BookManagement").Collection("cart_item")
}
