package test

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/joho/godotenv"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/bson"

	. "bookmanagement/config"
	. "bookmanagement/controllers"

	. "bookmanagement/middlewares"
	. "bookmanagement/models"
)

type UserTestSuite struct {
	suite.Suite
}

var (
	accessToken  string
	refreshToken string

	expiredAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDkzMDU2MTUsInVzZXJuYW1lIjoieW5lMTIzIn0.MxGxYiJMQkSEFbafd0Y62b7_lbnGKCMoY1CqH0S99Lc"
	resetToken         = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDk3Mzc0OTYsInVzZXJuYW1lIjoidGVzdGluZyJ9.2mD1SbDTX0ABydn3qb0_1IwSr34DBOPIWGacAbnlYkU"
)

func (suite *UserTestSuite) SetupSuite() {
	// Setup

	err := godotenv.Load("../.env")

	if err != nil {
		panic(err)
	}
	ConnectDB()
}

func (suite *UserTestSuite) SetupTest() {
	// Register
	user := User{
		Name:     "testing",
		Username: "testing",
		Email:    "damthixuany@gmail.com",
		Password: "123456",
	}
	userJSON, err := json.Marshal(user)
	if err != nil {
		assert.Fail(suite.T(), err.Error())
	}

	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/user/register", strings.NewReader(string(userJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(suite.T(), Register(c)) {
		assert.Equal(suite.T(), http.StatusCreated, rec.Code)
	}

	// login
	e = echo.New()

	req = httptest.NewRequest(echo.POST, "/user/login", strings.NewReader(`{"username":"testing","password":"123456"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)

	// Assertions
	if assert.NoError(suite.T(), Login(c)) {
		assert.Equal(suite.T(), http.StatusOK, rec.Code)

		// get token
		var token Token
		json.Unmarshal(rec.Body.Bytes(), &token)

		accessToken = token.AccessToken
		refreshToken = token.RefreshToken

	}
}

func (suite *UserTestSuite) TearDownTest() {
	// delete account
	_, err := GlobalUserCollection.DeleteOne(context.TODO(), bson.M{"username": "testing"})
	if err != nil {
		assert.Fail(suite.T(), err.Error())
	}

}

func (suite *UserTestSuite) TestRegisterSuccess() {
	user := User{
		Username: "testregister",
		Email:    "testing@gmail.com",
		Password: "testtest",
		Name:     "test register",
	}
	userJSON, err := json.Marshal(user)
	if err != nil {
		assert.Fail(suite.T(), err.Error())
	}

	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/user/register", strings.NewReader(string(userJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	// Assertions
	if assert.NoError(suite.T(), Register(c)) {
		assert.Equal(suite.T(), http.StatusCreated, rec.Code)
		// check user in db
		var insertResult InsertOneResult
		json.Unmarshal(rec.Body.Bytes(), &insertResult)

		var actualUser User
		if assert.NoError(suite.T(), GlobalUserCollection.FindOne(context.TODO(), bson.M{"_id": insertResult.InsertedID}).Decode(&actualUser)) {
			assert.Equal(suite.T(), user.Username, actualUser.Username)
			assert.Equal(suite.T(), user.Email, actualUser.Email)
			assert.Equal(suite.T(), user.Name, actualUser.Name)
		}

		// delete user
		_, err := GlobalUserCollection.DeleteOne(context.TODO(), bson.M{"_id": insertResult.InsertedID})
		if err != nil {
			assert.Fail(suite.T(), err.Error())
		}
	}

}

func (suite *UserTestSuite) TestRegisterFailDuplicatedUsername() {
	user := User{
		Username: "testing",
		Email:    "testing@gmail.com",
		Password: "testtest",
		Name:     "test register",
	}
	userJSON, err := json.Marshal(user)
	if err != nil {
		assert.Fail(suite.T(), err.Error())
	}

	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/user/register", strings.NewReader(string(userJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	// Assertions
	if assert.NoError(suite.T(), Register(c)) {
		assert.Equal(suite.T(), http.StatusBadRequest, rec.Code)
		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)
		assert.Equal(suite.T(), DuplicatedUsernameErrorMsg, errResponse.Message)

	}

}

func (suite *UserTestSuite) TestLoginSuccess() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/user/login", strings.NewReader(`{"username":"testing","password":"123456"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	// Assertions
	if assert.NoError(suite.T(), Login(c)) {
		assert.Equal(suite.T(), http.StatusOK, rec.Code)

		// get token
		var token Token
		json.Unmarshal(rec.Body.Bytes(), &token)

		accessToken = token.AccessToken
		refreshToken = token.RefreshToken
	}
}

func (suite *UserTestSuite) TestLoginFailIncorrectPassword() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/user/login", strings.NewReader(`{"username":"testing","password":"1234567"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	// Assertions
	if assert.NoError(suite.T(), Login(c)) {
		assert.Equal(suite.T(), http.StatusUnauthorized, rec.Code)

		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)
		assert.Equal(suite.T(), IncorrectPasswordErrorMsg, errResponse.Message)
	}
}

func (suite *UserTestSuite) TestLogoutSuccess() {
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/user/logout", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer "+accessToken)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetHandler(
		VerifyToken(Logout),
	)

	// Assertions
	if assert.NoError(suite.T(), c.Handler()(c)) {
		assert.Equal(suite.T(), http.StatusOK, rec.Code)
	}
}

func (suite *UserTestSuite) TestLogoutFailExpiredToken() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/user/logout", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer "+expiredAccessToken)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetHandler(
		VerifyToken(Logout),
	)

	// Assertions
	if assert.NoError(suite.T(), c.Handler()(c)) {
		assert.Equal(suite.T(), http.StatusUnauthorized, rec.Code)

		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), ExpiredTokenMsg, errResponse.Message)
	}
}
func (suite *UserTestSuite) TestLogoutFailInvalidSignatureToken() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/user/logout", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDk0NjM1OTAsInVzZXJuYW1lIjoidGVzdlluZyJ9.gIHcpWzDa-JY6kvEc4ny393pZ78igNp-HF0UVZveu-Y")

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetHandler(
		VerifyToken(Logout),
	)

	// Assertions
	if assert.NoError(suite.T(), c.Handler()(c)) {
		assert.Equal(suite.T(), http.StatusUnauthorized, rec.Code)

		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), InvalidSignature, errResponse.Message)
	}
}

func (suite *UserTestSuite) TestRefreshTokenSuccess() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/user/refresh_token", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer "+refreshToken)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	// Assertions
	if assert.NoError(suite.T(), RefreshToken(c)) {
		assert.Equal(suite.T(), http.StatusOK, rec.Code)

		// get token
		var token Token
		json.Unmarshal(rec.Body.Bytes(), &token)

		accessToken = token.AccessToken
		refreshToken = token.RefreshToken
	}
}

func (suite *UserTestSuite) TestRefreshTokenFailUnauthorized() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/user/refresh_token", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer "+refreshToken+"1")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	// Assertions
	if assert.NoError(suite.T(), RefreshToken(c)) {
		assert.Equal(suite.T(), http.StatusUnauthorized, rec.Code)
	}
}

func (suite *UserTestSuite) TestGetProfileSuccess() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/user/profile", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer "+accessToken)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetHandler(
		VerifyToken(GetProfile),
	)

	// Assertions
	if assert.NoError(suite.T(), c.Handler()(c)) {
		assert.Equal(suite.T(), http.StatusOK, rec.Code)

		var actualUser User
		json.Unmarshal(rec.Body.Bytes(), &actualUser)

		assert.Equal(suite.T(), "testing", actualUser.Username)
	}
}

func (suite *UserTestSuite) TestGetProfileFailUnauthorized() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.GET, "/user/profile", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer "+accessToken+"1")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetHandler(
		VerifyToken(GetProfile),
	)

	// Assertions
	if assert.NoError(suite.T(), c.Handler()(c)) {
		assert.Equal(suite.T(), http.StatusUnauthorized, rec.Code)

	}
}

func (suite *UserTestSuite) TestUpdateProfileSuccess() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.PUT, "/user/profile", strings.NewReader(`{"name":"testne"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer "+accessToken)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetHandler(
		VerifyToken(UpdateProfile),
	)

	// Assertions
	if assert.NoError(suite.T(), c.Handler()(c)) {
		assert.Equal(suite.T(), http.StatusAccepted, rec.Code)
	}

}

func (suite *UserTestSuite) TestUpdateProfileFailInvalidEmail() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.PUT, "/user/profile", strings.NewReader(`{"name":"testne","email":"testne"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer "+accessToken)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetHandler(
		VerifyToken(UpdateProfile),
	)

	// Assertions
	if assert.NoError(suite.T(), c.Handler()(c)) {
		assert.Equal(suite.T(), http.StatusBadRequest, rec.Code)

		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), InvalidEmailErrorMsg, errResponse.Message)
	}

}

func (suite *UserTestSuite) TestSendResetPasswordLinkSuccess() {
	var user = User{
		Username: "testing",
	}
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/user/:username/reset-password", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("username")
	c.SetParamValues(user.Username)

	if assert.NoError(suite.T(), SendResetPasswordLink(c)) {
		assert.Equal(suite.T(), http.StatusAccepted, rec.Code)
	}

}

func (suite *UserTestSuite) TestSendResetPasswordLinkFailNotfoundUsername() {
	var user = User{
		Username: "testtting",
	}
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/user/:username/reset-password", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("username")
	c.SetParamValues(user.Username)

	if assert.NoError(suite.T(), SendResetPasswordLink(c)) {
		assert.Equal(suite.T(), http.StatusNotFound, rec.Code)

		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), NotFoundUsernameErrorMsg, errResponse.Message)
	}

}

func (suite *UserTestSuite) TestResetPasswordSuccess() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.PUT, "/user/reset-password?reset_token="+resetToken, strings.NewReader(`{"new_password":"newpassword"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(suite.T(), ResetPassword(c)) {
		assert.Equal(suite.T(), http.StatusAccepted, rec.Code)

		// login with new password
		e := echo.New()
		req := httptest.NewRequest(echo.POST, "/user/login", strings.NewReader(`{"username":"testing","password":"newpassword"}`))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		// Assertions
		if assert.NoError(suite.T(), Login(c)) {
			assert.Equal(suite.T(), http.StatusOK, rec.Code)
		}

	}
}

func (suite *UserTestSuite) TestResetPasswordFail() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.PUT, "/user/reset-password?reset_token="+resetToken+"1", strings.NewReader(`{"new_password":"newpassword"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(suite.T(), ResetPassword(c)) {
		assert.Equal(suite.T(), http.StatusUnauthorized, rec.Code)

	}
}

func (suite *UserTestSuite) TestChangePasswordSuccess() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.PUT, "/user/change-password", strings.NewReader(`{"old_password":"123456","new_password":"newpassword"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer "+accessToken)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetHandler(
		VerifyToken(ChangePassword),
	)

	// Assertions
	if assert.NoError(suite.T(), c.Handler()(c)) {
		assert.Equal(suite.T(), http.StatusAccepted, rec.Code)

		// login with new password
		e := echo.New()
		req := httptest.NewRequest(echo.POST, "/user/login", strings.NewReader(`{"username":"testing","password":"newpassword"}`))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)

		// Assertions
		if assert.NoError(suite.T(), Login(c)) {
			assert.Equal(suite.T(), http.StatusOK, rec.Code)
		}
	}
}

func (suite *UserTestSuite) TestChangePasswordFailIncorrectPassword() {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(echo.PUT, "/user/change-password", strings.NewReader(`{"old_password":"1233456","new_password":"newpassword"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// set authentication for header
	req.Header.Set(echo.HeaderAuthorization, "Bearer "+accessToken)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetHandler(
		VerifyToken(ChangePassword),
	)

	// Assertions
	if assert.NoError(suite.T(), c.Handler()(c)) {
		assert.Equal(suite.T(), http.StatusBadRequest, rec.Code)

		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), IncorrectOldPasswordMsg, errResponse.Message)
	}
}

func TestUserTestSuite(t *testing.T) {
	suite.Run(t, new(UserTestSuite))
}
