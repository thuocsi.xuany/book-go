package test

import (
	. "bookmanagement/config"
	. "bookmanagement/controllers"
	. "bookmanagement/models"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"time"

	"context"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type BookTestSuite struct {
	suite.Suite
}

var (
	idBookTest  primitive.ObjectID
	bookTest    Book
	categoryIDs []primitive.ObjectID
	authorIDs   []primitive.ObjectID
	bookIDs     []primitive.ObjectID
)

// this function executes before the test suite begins execution
func (suite *BookTestSuite) SetupSuite() {
	ConnectDB()

	// clear all data in collection
	GlobalBookCollection.Drop(context.TODO())
	GlobalAuthorCollection.Drop(context.TODO())
	GlobalCategoryCollection.Drop(context.TODO())
	GlobalBookAuthorCollection.Drop(context.TODO())

	// category
	categoryIDs = []primitive.ObjectID{primitive.NewObjectID(), primitive.NewObjectID(), primitive.NewObjectID()}
	categoriesName := []string{"category1", "category2", "category3"}
	categoryDocs := []interface{}{
		Category{ID: categoryIDs[0], Name: &categoriesName[0], Description: "description 1", LastUpdate: time.Now()},
		Category{ID: categoryIDs[1], Name: &categoriesName[1], Description: "description 2", LastUpdate: time.Now()},
		Category{ID: categoryIDs[2], Name: &categoriesName[2], Description: "description 3", LastUpdate: time.Now()},
	}

	// author
	authorIDs = []primitive.ObjectID{primitive.NewObjectID(), primitive.NewObjectID(), primitive.NewObjectID()}
	authorsName := []string{"author 1", "author 2", "author 3"}
	authorDocs := []interface{}{
		Author{ID: authorIDs[0], Name: &authorsName[0], Avatar: "http://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50", LastUpdate: time.Now()},
		Author{ID: authorIDs[1], Name: &authorsName[1], Avatar: "https://iconarchive.com/download/i65802/hopstarter/bioman/Bioman-Avatar-5-Pink.ico", LastUpdate: time.Now()},
		Author{ID: authorIDs[2], Name: &authorsName[2], Avatar: "https://iconarchive.com/download/i65801/hopstarter/bioman/Bioman-Avatar-4-Yellow.ico", LastUpdate: time.Now()},
	}

	// book
	bookIDs = []primitive.ObjectID{primitive.NewObjectID(), primitive.NewObjectID(), primitive.NewObjectID()}
	booksName := []string{"book 1", "book 2", "book 3"}
	booksYear := []int{2000, 2001, 2002}
	bookDocs := []interface{}{
		Book{ID: bookIDs[0], Title: &booksName[0], Description: "description 1", Year: &booksYear[0], Price: 10, AuthorIDs: []primitive.ObjectID{authorIDs[0], authorIDs[2]}, CategoryIDs: []primitive.ObjectID{categoryIDs[0]}, Cover: "https://images-platform.99static.com//J7xMosdygEIVqk5FRXaT09mB2a0=/200x200:1800x1800/fit-in/500x500/99designs-contests-attachments/105/105433/attachment_105433385", LastUpdate: time.Now()},

		Book{ID: bookIDs[1], Title: &booksName[1], Description: "description 2", Year: &booksYear[1], Price: 20, AuthorIDs: []primitive.ObjectID{authorIDs[1]}, CategoryIDs: []primitive.ObjectID{categoryIDs[1], categoryIDs[2]}, Cover: "https://static.vecteezy.com/system/resources/thumbnails/000/214/945/small/motivational_book_cover.jpg", LastUpdate: time.Now()},

		Book{ID: bookIDs[2], Title: &booksName[2], Description: "description 3", Year: &booksYear[2], Price: 23, AuthorIDs: []primitive.ObjectID{authorIDs[2], authorIDs[1]}, CategoryIDs: []primitive.ObjectID{categoryIDs[2]}, Cover: "https://static.vecteezy.com/system/resources/thumbnails/000/217/193/small/motivational-book-cover-with-hand-lettering.jpg", LastUpdate: time.Now()},
	}

	// book author
	bookAuthorDocs := []interface{}{
		BookAuthor{BookID: bookIDs[0], AuthorID: authorIDs[0]},
		BookAuthor{BookID: bookIDs[0], AuthorID: authorIDs[2]},
		BookAuthor{BookID: bookIDs[1], AuthorID: authorIDs[1]},
		BookAuthor{BookID: bookIDs[2], AuthorID: authorIDs[2]},
		BookAuthor{BookID: bookIDs[2], AuthorID: authorIDs[1]},
	}

	GlobalCategoryCollection.InsertMany(context.TODO(), categoryDocs)
	GlobalAuthorCollection.InsertMany(context.TODO(), authorDocs)
	GlobalBookCollection.InsertMany(context.TODO(), bookDocs)
	GlobalBookAuthorCollection.InsertMany(context.TODO(), bookAuthorDocs)

	// create test data
	idBookTest = bookIDs[0]
	bookTest = bookDocs[0].(Book)
}

// this function executes after all tests executed
func (suite *BookTestSuite) TearDownSuite() {

}

// this function executes before each test case
func (suite *BookTestSuite) SetupTest() {
	// reset StartingNumber to one
}

// this function executes after each test case
func (suite *BookTestSuite) TearDownTest() {
}

func (suite *BookTestSuite) TestGetBooksByPageSuccess() {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/books?page=1", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(suite.T(), GetBooksByPage(c)) {
		assert.Equal(suite.T(), http.StatusOK, rec.Code)

		var books []Book
		if assert.NoError(suite.T(), json.Unmarshal(rec.Body.Bytes(), &books)) {
			assert.Equal(suite.T(), 2, len(books))

			assert.Equal(suite.T(), bookIDs[0], books[0].ID)
			assert.Equal(suite.T(), bookIDs[1], books[1].ID)
		}

	}

}
func (suite *BookTestSuite) TestGetBooksByPageFailExceededPage() {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/books?page=3", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(suite.T(), GetBooksByPage(c)) {
		assert.Equal(suite.T(), http.StatusNotFound, rec.Code)
	}

}

func (suite *BookTestSuite) TestGetBooksOfAuthorSuccess() {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/books/author", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("author_id")
	c.SetParamValues(authorIDs[1].Hex())

	if assert.NoError(suite.T(), GetBooksOfAuthor(c)) {
		assert.Equal(suite.T(), http.StatusOK, rec.Code)

		var books []Book
		if assert.NoError(suite.T(), json.Unmarshal(rec.Body.Bytes(), &books)) {
			assert.Equal(suite.T(), 2, len(books))

			expectedBookIDs := []primitive.ObjectID{bookIDs[1], bookIDs[2]}
			assert.Contains(suite.T(), expectedBookIDs, books[0].ID)
			assert.Contains(suite.T(), expectedBookIDs, books[1].ID)
		}

	}

}

func (suite *BookTestSuite) TestGetBooksOfCategorySuccess() {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/books/category", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("category_id")
	c.SetParamValues(categoryIDs[2].Hex())

	if assert.NoError(suite.T(), GetBooksOfCategory(c)) {
		assert.Equal(suite.T(), http.StatusOK, rec.Code)

		var books []Book
		if assert.NoError(suite.T(), json.Unmarshal(rec.Body.Bytes(), &books)) {
			assert.Equal(suite.T(), 2, len(books))

			expectedBookIDs := []primitive.ObjectID{bookIDs[2], bookIDs[1]}
			assert.Contains(suite.T(), expectedBookIDs, books[0].ID)
			assert.Contains(suite.T(), expectedBookIDs, books[1].ID)
		}

	}

}

func (suite *BookTestSuite) TestAddBookSuccess() {
	title := "book 4"
	year := 2020
	var bookAdd = Book{
		Title:       &title,
		Description: "description 4",
		Year:        &year,
		Price:       30,
		AuthorIDs:   []primitive.ObjectID{authorIDs[0], authorIDs[2]},
		CategoryIDs: []primitive.ObjectID{categoryIDs[1]},
		Cover:       "https://images-platform.99static.com/J7xMosdygEIVqk5FRXaT09mB2a0=/200x200:1800x1800/fit-in/500x500/99designs-contests-attachments/105/105433/attachment_105433385",
	}

	bookJSON, err := json.Marshal(bookAdd)
	assert.NoError(suite.T(), err)

	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/books", strings.NewReader(string(bookJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(suite.T(), AddBook(c)) {
		assert.Equal(suite.T(), http.StatusCreated, rec.Code)

		// check response body
		var insertResult InsertOneResult
		json.Unmarshal(rec.Body.Bytes(), &insertResult)

		var actualBook Book
		if assert.NoError(suite.T(), GlobalBookCollection.FindOne(context.TODO(), bson.M{"_id": insertResult.InsertedID}).Decode(&actualBook)) {
			CompareBook(suite.T(), bookAdd, actualBook)
		}

		// check book_author collection
		var authorIDsCheck []primitive.ObjectID
		cursor, err := GlobalBookAuthorCollection.Find(context.TODO(), bson.M{"book_id": insertResult.InsertedID})

		assert.NoError(suite.T(), err)
		defer cursor.Close(context.TODO())
		for cursor.Next(context.TODO()) {
			var bookAuthor BookAuthor
			err := cursor.Decode(&bookAuthor)
			assert.NoError(suite.T(), err)
			authorIDsCheck = append(authorIDsCheck, bookAuthor.AuthorID)
		}

		assert.Equal(suite.T(), len(authorIDsCheck), 2)
		assert.Contains(suite.T(), authorIDsCheck, authorIDs[0])
		assert.Contains(suite.T(), authorIDsCheck, authorIDs[2])
	}

}
func (suite *BookTestSuite) TestAddBookFailDuplicatedTitle() {

	title := "book 1"
	bookAdd := Book{
		Title:       &title,
		Description: "description 1",
	}

	bookJSON, err := json.Marshal(bookAdd)
	assert.NoError(suite.T(), err)

	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/books", strings.NewReader(string(bookJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(suite.T(), AddBook(c)) {
		assert.Equal(suite.T(), http.StatusBadRequest, rec.Code)

		// check response body
		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), DuplicatedTitleErrorMsg, errResponse.Message)
	}

}
func (suite *BookTestSuite) TestAddBookFailNonExistentAuthor() {
	title := "book 5"
	bookAdd := Book{
		Title:       &title,
		Description: "description 5",
		AuthorIDs:   []primitive.ObjectID{authorIDs[0], primitive.NewObjectID()},
	}

	bookJSON, err := json.Marshal(bookAdd)
	assert.NoError(suite.T(), err)

	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/books", strings.NewReader(string(bookJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(suite.T(), AddBook(c)) {
		assert.Equal(suite.T(), http.StatusBadRequest, rec.Code)

		// check response body
		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), NonExistentAuthorErrorMsg, errResponse.Message)
	}

}
func (suite *BookTestSuite) TestAddBookFailEmptyTitle() {
	title := ""
	bookAdd := Book{
		Description: "description 6",
		Title:       &title,
	}

	bookJSON, err := json.Marshal(bookAdd)
	assert.NoError(suite.T(), err)

	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/books", strings.NewReader(string(bookJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(suite.T(), AddBook(c)) {
		assert.Equal(suite.T(), http.StatusBadRequest, rec.Code)

		// check response body
		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), EmptyTitleErrorMsg, errResponse.Message)
	}

}
func (suite *BookTestSuite) TestAddBookFailMissingTitle() {

	bookAdd := Book{
		Description: "description 6",
	}

	bookJSON, err := json.Marshal(bookAdd)
	assert.NoError(suite.T(), err)

	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/books", strings.NewReader(string(bookJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(suite.T(), AddBook(c)) {
		assert.Equal(suite.T(), http.StatusBadRequest, rec.Code)

		// check response body
		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), MissingTitleErrorMsg, errResponse.Message)
	}

}
func (suite *BookTestSuite) TestGetBookSuccess() {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/books/:id", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("id")
	c.SetParamValues(idBookTest.Hex())

	if assert.NoError(suite.T(), GetBook(c)) {
		assert.Equal(suite.T(), http.StatusOK, rec.Code)

		var resBook Book
		if assert.NoError(suite.T(), json.Unmarshal(rec.Body.Bytes(), &resBook)) {
			assert.Equal(suite.T(), bookTest.ID, resBook.ID)
			CompareBook(suite.T(), bookTest, resBook)
		}
	}
}

func (suite *BookTestSuite) TestGetBookFailNotfoundID() {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/books/:id", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("id")
	c.SetParamValues(primitive.NewObjectID().Hex())

	if assert.NoError(suite.T(), GetBook(c)) {
		assert.Equal(suite.T(), http.StatusNotFound, rec.Code)

		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), NotFoundBookIDErrorMsg, errResponse.Message)
	}

}
func (suite *BookTestSuite) TestGetBookFailInvalidID() {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/books/:id", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("id")
	c.SetParamValues("invalid-id")

	if assert.NoError(suite.T(), GetBook(c)) {
		assert.Equal(suite.T(), http.StatusBadRequest, rec.Code)

		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), InvalidIDErrorMsg, errResponse.Message)
	}

}

func (suite *BookTestSuite) TestUpdateBookSuccess() {
	title := "book update"
	bookUpdate := Book{
		Title:       &title,
		Description: "description update",
		AuthorIDs:   []primitive.ObjectID{authorIDs[2]},
		CategoryIDs: []primitive.ObjectID{categoryIDs[2]},
	}

	bookJSON, err := json.Marshal(bookUpdate)
	assert.NoError(suite.T(), err)

	e := echo.New()
	req := httptest.NewRequest(http.MethodPut, "/books/:id", strings.NewReader(string(bookJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("id")
	c.SetParamValues(idBookTest.Hex())

	if assert.NoError(suite.T(), UpdateBook(c)) {
		assert.Equal(suite.T(), http.StatusAccepted, rec.Code)

		// check response body
		var updateResult UpdateOneResult
		if assert.NoError(suite.T(), json.Unmarshal(rec.Body.Bytes(), &updateResult)) {
			assert.Equal(suite.T(), int64(1), updateResult.MatchedCount)
			assert.Equal(suite.T(), int64(1), updateResult.ModifiedCount)

			// check data update
			var resBook Book
			err := GlobalBookCollection.FindOne(context.TODO(), bson.M{"_id": idBookTest}).Decode(&resBook)
			assert.NoError(suite.T(), err)

			assert.Equal(suite.T(), bookUpdate.Title, resBook.Title)
			assert.Equal(suite.T(), bookUpdate.Description, resBook.Description)
			assert.Equal(suite.T(), bookUpdate.AuthorIDs, resBook.AuthorIDs)
			assert.Equal(suite.T(), bookUpdate.CategoryIDs, resBook.CategoryIDs)
		}

		// check book_author collection
		var authorIDsCheck []primitive.ObjectID
		cursor, err := GlobalBookAuthorCollection.Find(context.TODO(), bson.M{"book_id": idBookTest})

		assert.NoError(suite.T(), err)
		defer cursor.Close(context.TODO())
		for cursor.Next(context.TODO()) {
			var bookAuthor BookAuthor
			err := cursor.Decode(&bookAuthor)
			assert.NoError(suite.T(), err)
			authorIDsCheck = append(authorIDsCheck, bookAuthor.AuthorID)
		}

		assert.Equal(suite.T(), len(authorIDsCheck), 1)
		assert.Contains(suite.T(), authorIDsCheck, authorIDs[2])
	}

}

func (suite *BookTestSuite) TestUpdateBookFailNotfoundID() {
	title := "book update"
	bookUpdate := Book{
		Title:       &title,
		Description: "description update",
	}

	bookJSON, err := json.Marshal(bookUpdate)
	assert.NoError(suite.T(), err)

	e := echo.New()
	req := httptest.NewRequest(http.MethodPut, "/books/:id", strings.NewReader(string(bookJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("id")
	c.SetParamValues(primitive.NewObjectID().Hex())

	if assert.NoError(suite.T(), UpdateBook(c)) {
		assert.Equal(suite.T(), http.StatusNotFound, rec.Code)

		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), NotFoundBookIDErrorMsg, errResponse.Message)
	}

}

func (suite *BookTestSuite) TestDeleteBookSuccess() {
	// delete => check bookauthor
	e := echo.New()
	req := httptest.NewRequest(http.MethodDelete, "/books/:id", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("id")
	c.SetParamValues(idBookTest.Hex())

	if assert.NoError(suite.T(), DeleteBook(c)) {
		assert.Equal(suite.T(), http.StatusAccepted, rec.Code)

		var deleteResult DeleteOneResult
		if assert.NoError(suite.T(), json.Unmarshal(rec.Body.Bytes(), &deleteResult)) {
			assert.Equal(suite.T(), int64(1), deleteResult.DeletedCount)

			// check already deleted
			// in Book collection
			filter := bson.M{"_id": idBookTest}
			var deletedBook Book
			assert.EqualError(suite.T(), GlobalBookCollection.FindOne(context.TODO(), filter).Decode(&deletedBook), mongo.ErrNoDocuments.Error())

			// in BookAuthor collection
			filter = bson.M{"book_id": idBookTest}
			var deletedBookAuthor BookAuthor
			assert.EqualError(suite.T(), GlobalBookAuthorCollection.FindOne(context.TODO(), filter).Decode(&deletedBookAuthor), mongo.ErrNoDocuments.Error())
		}
	}

}

func (suite *BookTestSuite) TestDeleteBookFailNotfoundID() {
	e := echo.New()
	req := httptest.NewRequest(http.MethodDelete, "/books/:id", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	c.SetParamNames("id")
	c.SetParamValues(primitive.NewObjectID().Hex())

	if assert.NoError(suite.T(), DeleteBook(c)) {
		assert.Equal(suite.T(), http.StatusNotFound, rec.Code)

		var errResponse Response
		json.Unmarshal(rec.Body.Bytes(), &errResponse)

		assert.Equal(suite.T(), NotFoundBookIDErrorMsg, errResponse.Message)
	}

}

func TestBookTestSuite(t *testing.T) {
	suite.Run(t, new(BookTestSuite))
}

func CompareBook(t *testing.T, expectedBook Book, actualBook Book) {
	// assert.Equal(t, expectedBook.ID, actualBook.ID)
	assert.Equal(t, expectedBook.Title, actualBook.Title)
	assert.Equal(t, expectedBook.Description, actualBook.Description)
	assert.Equal(t, expectedBook.Year, actualBook.Year)
	assert.Equal(t, expectedBook.Price, actualBook.Price)
	assert.Equal(t, len(expectedBook.AuthorIDs), len(actualBook.AuthorIDs)) // check arrray
	assert.Equal(t, len(expectedBook.CategoryIDs), len(actualBook.CategoryIDs))
	assert.Equal(t, expectedBook.Cover, actualBook.Cover)
}
