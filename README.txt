connect mongoDB compass: 
	"mongodb+srv://ydam:ydam@notes-cluster.iwril.mongodb.net/test?authSource=admin&replicaSet=atlas-y85dtt-shard-0&readPreference=primary&ssl=true"

# book-go


get all book of author
		all book of author
		books	by page		=> success => check 
						        => fail <= page exceeded


add book:	(return objectID)
	success => check response + db of book_author 
	fail <= duplicated title
	fail <= non existent category/author
	fail <= empty title
	fail <= missing title

get book : return ref data
	success => check response
	fail <= not found ID
	fail <= invalid ID
	
update book	~
	success => check response + db of book_author 
	fail <= non existent category/author
	fail <= not found ID
	
delete book:
	success => check db of book + book_author 
	fail <= not found ID
	fail <= invalid ID



TestChangePassword - Fail - IncorrectPassword
TestChangePassword - Success
TestGetProfile - Fail - Unauthorized
TestGetProfile - Success
TestLoginSuccess
TestLogin - Fail - IncorrectPassword
TestLogout- Fail - ExpiredToken
TestLogout- Fail - InvalidSignatureToken
TestLogout - Success
TestRefreshToken - Fail - Unauthorized
TestRefreshToken - Success
TestRegister - Fail - DuplicatedUsername
TestRegister - Success
TestResetPassword - Fail - 
TestResetPassword - Success
TestSendResetPasswordLink - Fail - NotfoundUsername
TestSendResetPasswordLink - Success
TestUpdateProfile - Fail - InvalidEmail
TestUpdateProfile - Success